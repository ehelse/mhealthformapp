cordova build --release android

cd platforms/android/ant-build

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "D:\LarsKristian\Documents\Dropbox\AndroidKeystore\mobilars-release-key.keystore" mHealthFormApp-release-unsigned.apk mobilars

cp mHealthFormApp-release-unsigned.apk mHealthFormApp.apk
scp mHealthFormApp.apk larsr@tgs-fhir-web.testsenter.nhn.no:/usr/share/nginx/html/apk

cd ../../..
