/**
 * Created by LarsKristian on 05.11.2015.
 */


//app.controller('LoginController', function MainCtrl($scope, $q, $document) {'
app.controller('LoginController', ['$scope', '$document','$location','$http', 'Patient',
    function($scope, $document, $location, $http, Patient) {

    console.log('LoginController .Location='+JSON.stringify($location));

    $scope.accessToken = sessionStorage.getItem('fhirAccessToken');
    console.log('$scope.accessToken = '+$scope.accessToken );

    $scope.message = "You need to log in to the service and authorize this application to submit health data on your behalf.";

    $scope.loginInAppBrowser = function () {
        console.log('Logging in using inappbrowser');
        $scope.inAppBrowser();
    };

    $scope.saveToken = function() {
        console.log('Logging in with manual token');
        $http.defaults.headers.common['Authorization']= 'Bearer '+$scope.accessToken;
        $scope.message = "Logged in";
        $location.path('/loggedin', false);
    };


    $scope.makeLink = function () {

        console.log("Making link...");

        sessionStorage.setItem("cordovaAppLocation", $location.absUrl());
        $scope.localUrl = $location.absUrl();

        var client_id = 'fhirclient';
        var redirect_uri = 'http://apps.ehelselab.com/client/';
        //var redirect_uri = $location.absUrl();
        //var redirect_uri = "http://localhost:8000/index.html#/loggedin";
        var response_type = "token"; //"token";
        var scope = "patient/*.*";
        var nonce = "6130755535249629";
        var state = Date.now() + "" + Math.random();
        localStorage["state"] = state;

        $scope.authUrl =
            //AUTH_URL + "?response_mode=form_post&" +
            AUTH_URL + "?" +
            "client_id=" + encodeURI(client_id) + "&" +
            //"redirect_uri=" + encodeURI(redirect_uri).replace('index.html#/','launch.html') + "&" +'
            "redirect_uri=" + encodeURI(redirect_uri) + "&" +
            "response_type=" + encodeURI(response_type) + "&" +
            "scope=" + encodeURI(scope) + "&" +
            "nonce="+ encodeURI(nonce) + "&" +
            "state=" + encodeURI(state);

    };

    $scope.inAppBrowser = function() {
        //file:///android_asset/www/launch.html
        /*
         http://tgs-fhir-web.testsenter.nhn.no/lars/phaclient/
         #access_token=eyJ0...30.MQywuPU_GQ&token_type=Bearer&expires_in=36000&scope=Patient%2FObservation.create&state=14468169053500.1095023569650948
         */
        console.log("Opening in-app-browser");
        var ref = cordova.InAppBrowser.open($scope.authUrl, '_blank', 'location=no,toolbar=no');
        //var ref = cordova.InAppBrowser.open($scope.authUrl, '_blank', 'location=yes');
        ref.addEventListener('loadstart', function(event) { console.log(event.url);
            if (event.url.indexOf('access_token=') != -1) {
                ref.close();
                var accessToken = event.url.substring(event.url.indexOf('access_token=')+13);
                accessToken = accessToken.substr(0,accessToken.indexOf('&'));
                console.log('accessToken='+accessToken);
                sessionStorage.setItem('fhirAccessToken', accessToken);
                $scope.$apply(function () {
                    $http.defaults.headers.common['Authorization']= 'Bearer '+accessToken;
                    //$httpProvider.defaults.useXDomain = true;
                    //delete $httpProvider.defaults.headers.common['X-Requested-With'];
                    $location.path('/loggedin', false);
                });

            }
        });
    };

    $document.ready(function(){
        console.log('Doc ready');
        $scope.makeLink();
        //$scope.authenticate();
    });

}]);