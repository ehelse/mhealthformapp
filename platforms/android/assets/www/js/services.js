'use strict';

/* Services */

var fhirServices = angular.module('fhirServices', ['ngResource']);

// /Observation?subject%3APatient=1032702&code=149530%2C150456"
fhirServices.factory("Observation", function ($resource) {

    /*
     var settings = JSON.parse(localStorage.getItem("fhirSettings"));
     if (typeof settings == undefined) {
     console.log('Settings not set')
     settings = { fhirUrl: FHIR_URL };
     }
     */
    var settings = { fhirUrl: FHIR_URL };

    return $resource(
        settings.fhirUrl+'/Observation/:id',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            'update': { method:'PUT' }
        }
    );
});

fhirServices.factory("Patient", function ($resource) {

    /*
     var settings = JSON.parse(localStorage.getItem("fhirSettings"));
     if (typeof settings == undefined) {
     console.log('Settings not set')
     settings = { fhirUrl: FHIR_URL };
     }
     */
    var settings = { fhirUrl: FHIR_URL };

    return $resource(
        settings.fhirUrl+'/Patient/:id',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            'update': { method:'PUT' }
        }
        ,
        {
            'delete': { method:'DELETE' }
        }
    );
});

fhirServices.factory("Questionnaire", function ($resource) {

    var settings = { fhirUrl: FHIR_URL };

    return $resource(
        settings.fhirUrl+'/Questionnaire/:id',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        }
    );
});

fhirServices.factory("QuestionnaireResponse", function ($resource) {

    var settings = { fhirUrl: FHIR_URL };

    return $resource(
        settings.fhirUrl+'/QuestionnaireResponse/:id',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        }
    );
});