﻿//console.clear(); // <-- keep the console clean on refresh

var app = angular.module('mHealthFormApp', ['ngRoute', 'formly', 'formlyBootstrap', 'Converter', 'fhirServices'],
    function config(formlyConfigProvider) {
        // set templates here
        formlyConfigProvider.setType({
            name: 'custom',
            templateUrl: 'custom.html'
        });
    });


app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/', {templateUrl: 'partials/login.html', controller: 'LoginController'}).
            when('/loggedin', {templateUrl: 'partials/list.html', controller: 'ListController'}).
            when('/form/:formId', {templateUrl: 'partials/form.html', controller: 'FormController'}).
            when('/view/:formId', {templateUrl: 'partials/view.html', controller: 'ResponseController'}).
            otherwise({
                redirectTo: '/loggedin'
            });
    }]);


app.controller('ListController', function MainCtrl($scope, $http, Questionnaire, QuestionnaireResponse, Patient) {

    console.log('ListController is called');
    $scope.accessToken = sessionStorage.getItem('fhirAccessToken');
    $http.defaults.headers.common['Authorization']= 'Bearer '+$scope.accessToken;

    $scope.patient = Patient.get({}, function () {
        localStorage.setItem("fhirPatient", $scope.patient.entry[0].resource.id);
        $scope.patientId = $scope.patient.entry[0].resource.id;
        localStorage.setItem("fhirPatientResource", $scope.patient.entry[0].resource);
        console.log('$scope.patient='+JSON.stringify($scope.patient));
        $scope.message = "Logged in";
        console.log('Patient='+$scope.Patient);
    });

    $scope.questionnareOverview = {};
    $scope.questionnaireList = Questionnaire.get({}, function () {
        console.log('$scope.questionnaire=' + JSON.stringify($scope.questionnaire));
        var entries = $scope.questionnaireList.entry;
        var arrayLength = entries.length;
        for (var i = 0; i < arrayLength; i++) {
            var id = entries[i].resource.id;
            var text = entries[i].resource.group.title;
            $scope.questionnareOverview[id] = text;
            }
        console.log('$scope.questionnareOverview='+JSON.stringify($scope.questionnareOverview));
    });

    $scope.questionnaireResponses = QuestionnaireResponse.get({'_sort:desc':'_lastUpdated'}, function () {
        console.log('$scope.questionnaireResponses=' + JSON.stringify($scope.questionnaire));
    });

    $scope.getFormDescription = function(ref) {
        return $scope.questionnareOverview[ref.replace('Questionnaire/','')];
    };

});


app.controller('ResponseController', function MainCtrl($scope, $routeParams, QuestionnaireResponse) {

    $scope.accessToken = sessionStorage.getItem('fhirAccessToken');
    $http.defaults.headers.common['Authorization']= 'Bearer '+$scope.accessToken;
    $scope.patientId = localStorage.getItem("fhirPatient");
    $scope.questionnaireResponse = QuestionnaireResponse.get({id: $routeParams.formId}, function () {
        console.log('$scope.questionnaireResponse=' + JSON.stringify($scope.questionnaire));
    });
});

app.controller('FormController', function MainCtrl($scope, $http, formlyVersion, FHIR2Formly, Formly2FHIR, $location, $routeParams, Questionnaire, QuestionnaireResponse) {

    $scope.accessToken = sessionStorage.getItem('fhirAccessToken');
    $http.defaults.headers.common['Authorization']= 'Bearer '+$scope.accessToken;

    $scope.patientId = localStorage.getItem("fhirPatient");

    console.log('MainCtrl:' + JSON.stringify($location));
    console.log('$routeParams:' + JSON.stringify($routeParams));

    $scope.alert = {};

    $scope.selectedFormId = $routeParams.formId;

    $scope.viewForm = function (formId) {
        console.log('formId:' + formId);
        $scope.selectedFormId = formId;
        $scope.questionnaire = Questionnaire.get({id: formId, _format: 'json'}, function () {
            console.log('$scope.questionnaire=' + JSON.stringify($scope.questionnaire));
            //$scope.questionnaireString = JSON.stringify($scope.);
            FHIR2Formly.convertFromFHIR($scope.questionnaire);
        });
    };

    $scope.viewForm($routeParams.formId);

    $scope.generatedFormFields = FHIR2Formly.getFormly();

    // variable assignment
    $scope.author = { // optionally fill in your info below :-)
        name: 'Lars Roland',
        url: 'https://http://tgs-fhir-web.testsenter.nhn.no/' // a link to your twitter/github/blog/whatever
    };

    $scope.env = {
        angularVersion: angular.version.full,
        formlyVersion: formlyVersion
    };

    $scope.model = {};

    $scope.saveAnswer = function () {
        console.log('saveAnswer');
        $scope.questionnaireAnswer = Formly2FHIR.createQuestionnaireAnswer($scope.questionnaire, $scope.model, $scope.patientId);
        console.log('saveAnswer result ' + JSON.stringify($scope.questionnaireAnswer));
        QuestionnaireResponse.save({}, $scope.questionnaireAnswer, function (msg) {
            console.log('Saved QuestionnaireResponse OK:' + JSON.stringify(msg));
            $scope.alert.ok = 'Success:'+msg.issue[0].diagnostics;
            $scope.alert.error = '';
        }, function (msg) {
            console.log('Saved QuestionnaireResponse error:' + JSON.stringify(msg));
            $scope.alert.ok = '';
            $scope.alert.error = 'Failed saving response: '+ JSON.stringify(msg);
        });
    };


});


