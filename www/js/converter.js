/**
 * Created by LarsKristian on 02.09.2015.
 */

var converterService = angular.module('Converter', []);

converterService.service('FHIR2Formly', function () {

    var converter = this;

    converter.createFormlyModel = function (json, formlyForm) {
        if (typeof formlyForm === 'undefined') {
            formlyForm = [];
        }

        if (typeof json.concept !== 'undefined') {
            var header = {
                template: '<h2>'+json.concept[0].display+'</h2>'
            };
            formlyForm.push(header);
        }

        if (typeof json.group !== 'undefined') {
            //.log('Found group: ' + JSON.stringify(json));
            // the group variable is defined and contains an array
            return converter.createFormlyModel(json.group, formlyForm);
        } else {
            //console.log('Did not find group: ' + JSON.stringify(json));
            return converter.expandGroup(json, formlyForm);
        }
    };
    converter.expandGroup = function (json, formlyForm) {
        if (Array.isArray(json)) {
            //console.log('Found an array: ' + JSON.stringify(json));
            var arrayLength = json.length;
            //console.log('Arraylength: ' + arrayLength);

            for (var i = 0; i < arrayLength; i++) {
                //console.log('Array-Item[' + i + ']' + JSON.stringify(json[i]));

                if (typeof json[i].text !== 'undefined') {
                    var header = {
                        template: '<h3>'+json[i].text+'</h3>'
                    };
                    formlyForm.push(header);
                }

                if (typeof json[i].title !== 'undefined') {
                    var header = {
                        template: '<h3>'+json[i].title+'</h3>'
                    };
                    formlyForm.push(header);
                }

                if (typeof json[i].question !== 'undefined') {
                    var questionArray = json[i].question;
                    var questionArrayLength = json[i].question.length
                    //console.log('Array has a question array of ' + questionArrayLength + ' items.' + JSON.stringify(questionArray));
                    for (var qi = 0; qi < questionArrayLength; qi++) {
                        var questionArrayItem = questionArray[qi];
                        console.log('questionArrayItem='+JSON.stringify(questionArrayItem));
                        var question = {};
                        var rand = Math.floor((Math.random() * 1000000) + 1);
                        if (typeof questionArrayItem.linkId !== 'undefined') {
                            console.log('Reusing linkid, but removing spaces and ?');
                            question.key = questionArrayItem.linkId.replace(" ","").replace('?', '');
                            questionArrayItem.linkId = question.key;
                        }
                        else {
                            console.log('Creating new linkid based on question text');
                            question.key = questionArrayItem.text.split(" ").join("").replace('?', '');
                            questionArrayItem.linkId = question.key;
                        }

                        question.templateOptions = {};

                        if (typeof questionArrayItem.type !== 'undefined') {
                            //console.log('Type set to '+questionArrayItem.type);
                            if (questionArrayItem.type == 'integer') {
                                question.templateOptions.type = 'number';
                                question.type = 'input';
                            } else if (questionArrayItem.type == 'date') {
                                question.templateOptions.type = 'date';
                                question.type = 'input';
                            } else if (questionArrayItem.type == 'boolean') {
                                question.templateOptions.type = 'checkbox';
                                question.type = 'checkbox';
                            } else if (questionArrayItem.type == 'choice') {
                                question.templateOptions.type = 'choice';
                                question.type = 'select';

                                var containedArray = converter.questionnaire.contained;
                                if (typeof questionArrayItem.options !== undefined && typeof containedArray !== 'undefined') {
                                    var containedArrayLength = containedArray.length;
                                    //console.log('Contain array has a question array of ' + containedArrayLength + ' items.');
                                    for (var ci = 0; ci < containedArrayLength; ci++) {
                                        var contained = containedArray[ci];
                                        if (contained.id == questionArrayItem.options.reference) {
                                            console.log('Found concept:' + contained.id);
                                            var fhirOptArray = contained.compose.include[0].concept;
                                            var options = [];
                                            var optionArrayLength = fhirOptArray.length;
                                            //console.log('Array has a question array of ' + questionArrayLength + ' items.' + JSON.stringify(questionArray));
                                            for (var oi = 0; oi < optionArrayLength; oi++) {
                                                var singleOpt = {};
                                                var fhirOpt = fhirOptArray[oi];
                                                singleOpt.name = fhirOpt.display;
                                                singleOpt.value = fhirOpt.code;
                                                options.push(singleOpt);
                                            }
                                            question.templateOptions.options = options;
                                            console.log("Options:"+JSON.stringify(options));
                                        }
                                    }
                                } else if (typeof questionArrayItem.option !== 'undefined') {
                                    var optionLength = questionArrayItem.option.length;
                                    var options = [];
                                    console.log('singleOpt.valueThis has '+optionLength+' options.');
                                    for (var oi = 0; oi < optionLength; oi++) {
                                        var singleOpt = {};
                                        singleOpt.value = questionArrayItem.option[oi].code;
                                        singleOpt.name = questionArrayItem.option[oi].display;
                                        options.push(singleOpt);
                                    }
                                    question.templateOptions.options = options;
                                    console.log("Options:"+JSON.stringify(options));
                                }

                            } else {
                                question.templateOptions.type = questionArrayItem.type;
                                question.type = 'input';
                            }
                        }
                        else {
                            question.templateOptions.type = 'text';
                            question.type = 'input';
                        }

                        question.templateOptions.label = questionArrayItem.text;
                        question.templateOptions.placeholder = questionArrayItem.text;
                        question.templateOptions.required = false;
                        //question.question = questionArrayItem.text;
                        formlyForm.push(question);
                        //console.log('Adding a question: ' + questionArrayItem.text);
                    }
                }
            }

        } else {
            //console.log('Did not find an array: ' + JSON.stringify(json));
            //return formlyForm;
        }

        return formlyForm;

    };

    converter.formly = {};
    converter.formly.data = [];

    converter.getFormly = function() {
        return converter.formly;
    };

    converter.convertFromFHIR = function (json) {
        converter.questionnaire = json;
        converter.formly.data = converter.createFormlyModel(converter.questionnaire);
        return converter.formly;
    };

});


converterService.service('Formly2FHIR', function () {

    var converter = this;

    // Create QuestionnaireAnswer
    converter.createQuestionnaireAnswer = function(questionnaire, answers, patientId) {

        var html = '<div><table>';

        converter.questionnaire = questionnaire;
        converter.model = answers;
        console.log('Creating answer');
        converter.questionnaireAnswerString = JSON.stringify(converter.questionnaire);
        converter.questionnaireAnswer = JSON.parse(converter.questionnaireAnswerString);

        // Remove unwanted, and required.

        converter.questionnaireAnswer.resourceType = 'QuestionnaireResponse';
        converter.questionnaireAnswer.questionnaire = { reference: 'Questionnaire/'+converter.questionnaireAnswer.id };
        delete converter.questionnaireAnswer.id;
        converter.questionnaireAnswer.status = 'completed';
        //converter.questionnaireAnswer.text = converter.formatText(answers);
        converter.questionnaireAnswer.subject = {"reference": "Patient/"+patientId};
        converter.questionnaireAnswer.author = {"reference": "Patient/"+patientId};
        converter.questionnaireAnswer.authored = new Date();
        converter.questionnaireAnswer.source = {"reference": "Patient/"+patientId};
        delete converter.questionnaireAnswer.publisher;
        delete converter.questionnaireAnswer.telecom;
        delete converter.questionnaireAnswer.meta;

        // Now traverse the structure to delete more unwanted elements: Remove concept, type, option, options
        if (typeof converter.questionnaireAnswer.concept !== 'undefined') {
            console.log('deleting concept');
            delete converter.questionnaireAnswer.concept;
        }
        if (typeof converter.questionnaireAnswer.group !== 'undefined') {
            console.log('Found group:' + JSON.stringify(converter.questionnaireAnswer.group));
            // the group variable is defined and contains an array
            converter.questionnaireAnswer.group = converter.checkGroup(converter.questionnaireAnswer.group, answers, html);
        }

        html = html + '</table></div>';
        converter.questionnaireAnswer.text = html;
        return converter.questionnaireAnswer;
    };

    converter.checkGroup = function(json, answers, html) {
        if (Array.isArray(json)) {
            console.log('checkGroup Found an array: ' + JSON.stringify(json));
            var arrayLength = json.length;
            for (var i = 0; i < arrayLength; i++) {
                console.log('Array-Item[' + i + ']' + JSON.stringify(json[i]));
                if (typeof json[i].question !== 'undefined') {
                    json[i].question = converter.checkAnswerArray(json[i].question, answers, html);
                }
            }
        } else if (typeof json.group !== 'undefined') {
            console.log('Array not found');

            if (typeof json.concept !== 'undefined') {
                console.log('Deleting concept');
                delete json.concept;
            }

            json.group = converter.checkGroup(json.group, answers, html);
        }
        return json;
    };

    converter.checkAnswerArray = function(json, answers, html) {

        if (Array.isArray(json)) {
            console.log('checkGroup Found an array: ' + JSON.stringify(json));
            var questionArrayLength = json.length;

            for (var q = 0; q < questionArrayLength; q++) {
                console.log('Array-Item[' +q + ']' + JSON.stringify(json[q]));

                if (typeof json[q].type !== 'undefined' && typeof json[q].linkId !== 'undefined') {

                    if (typeof answers[json[q].linkId] !== 'undefined') {
                        console.log('Inserting answer '+answers[json[q].linkId]);
                        var a = answers[json[q].linkId];
                        html = html + '<tr><td>'+json[q].text+'</td><td>'+a+'</td></tr>';

                        if (json[q].type === 'choice') {
                            json[q].answer = [];
                            // TODO: convey display and system
                            var coding = { valueCoding: { code: a, system: 'unknownSystem', display: a }};
                            json[q].answer.push(coding);
                        } else if (json[q].type === 'string') {
                            json[q].answer = [];
                            var value = { valueString: a };
                            json[q].answer.push(value);
                        } else if (json[q].type === 'date') {
                            json[q].answer = [];
                            var value = { valueDate: a };
                            json[q].answer.push(value);
                        } else if (json[q].type === 'integer') {
                            json[q].answer = [];
                            var value = { valueInteger: a };
                            json[q].answer.push(value);
                        }
                    } else {
                        console.log('No answer for :'+json[q].linkId);
                    }
                }
                else {
                    console.log('ERROR; The question must have linkId and type');
                }

                console.log('Delete +json[q].type');
                delete json[q].type;

                if (typeof json[q].option !== 'undefined') {
                    console.log('Delete +json[q].option');
                    delete json[q].option;
                }
                if (typeof json[q].options !== 'undefined') {
                    console.log('Delete +json[q].options');
                    delete json[q].options;
                }
            }
        }
        return json;

    };

    converter.formatText = function(answers) {
        var str = JSON.stringify(answers);
        return '<pre>'+str.replace('"','').replace('{','').replace('}','')+'</pre>';
    };

});