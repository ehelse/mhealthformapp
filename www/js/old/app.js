/**
 * Created by LarsKristian on 18.08.2015.
 */


// Declare app level module which depends on filters, and services

var fhirApp = angular.module('fhirApp', [
    'ngRoute',
    'fhirApp.services',
    'fhirApp.controllers'
]);

fhirApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {templateUrl: 'partials/partial_main.html'});
    $routeProvider.when('/settings', {templateUrl: 'partials/partial_settings.html'});
    $routeProvider.otherwise({redirectTo: '/'});

}]);

/*
angular.element(document).ready(function() {
    angular.bootstrap(document, ["myFhirApp"]);
});
*/

/*
fhirApp.run(function ($rootScope) {
    // bleservice.connect();
    console.log('fhirApp.run');
});
*/