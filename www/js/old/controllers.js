﻿//var FHIR_URL = 'http://mobilars.apphb.com/fhir';
var FHIR_URL = 'http://fhir.roland.bz';


angular.module('fhirApp.controllers', [])
    .controller('ObsController', ['$scope', '$q', 'BLE', 'Settings',
    function ($scope, $q, BLE, Settings) {

        $scope.settings = Settings.getSettings();

        $scope.BLE = BLE;

        $scope.sensorValues = BLE.getSensorValues();
        $scope.sensorMessage = BLE.getSensorMessage();

        $scope.state = Math.round(Math.random() * 100000000).toString();

// Wait for device API libraries to load
//
        document.addEventListener("deviceready", onDeviceReady, false);

// device APIs are available
//
        function onDeviceReady() {

            console.log('onDeviceReady settings:'+JSON.stringify($scope.settings));

            $scope.scan();

            $scope.accessToken = Settings.settings.accessToken;
            var defer = $q.defer();
            //$scope.checkAuthentication(defer);
            $scope.getPatientInfo(defer);
            defer.promise
                .then(function () {
                    console.log('Authentication token still valid');
                },
                function (error) {
                    console.log("Error during authentication. Token not valid? " + error);
                    $scope.reAuthenticate();
                });

        }

        //var ref = cordova.InAppBrowser.open('http://apache.org', '_blank', 'location=yes');

        $scope.reAuthenticate = function() {

            $scope.authUrl = $scope.settings.authUri + "?" +
            "response_type=code&" +
            "client_id=" + encodeURIComponent($scope.settings.clientId) + "&" +
            "scope=" + encodeURIComponent(Settings.getScope()) + "&" +
            "redirect_uri=" + encodeURIComponent($scope.settings.redirectUri) + "&" +
            "aud=" + encodeURIComponent($scope.settings.serviceUri) +
            "&state=" + $scope.state;

            console.log('onDeviceReady authUrl:' + JSON.stringify($scope.authUrl));

            var ref = window.open($scope.authUrl, '_blank', 'location=no,toolbar=no');
            ref.addEventListener('loadstart', function (event) {

                var url = event.url;
                var queryString = {};
                url.replace(
                    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
                    function ($0, $1, $2, $3) {
                        queryString[$1] = $3;
                    }
                );

                if (queryString['error']) {
                    alert('Error!');
                    ref.close();
                }

                if (queryString['code'] && event.url.indexOf("SamveisWebapp") > -1) {
                    console.log('code=' + queryString['code'] + ' and redirected to Samveis App');
                    var code = queryString['code'];
                    ref.close();

                    // TODO: Move to Angular. See formly-fhir project
                    $.post($scope.settings.tokenUri,
                        {
                            code: code,
                            grant_type: 'authorization_code',
                            redirect_uri: $scope.settings.redirectUri,
                            client_id: $scope.settings.clientId
                        },
                        function (res) {
                            // should get back the access token and the patient ID
                            $scope.accessToken = res.access_token;
                            Settings.settings.accessToken = $scope.accessToken;
                            Settings.saveSettings();

                            $scope.patientId = $scope.settings.patientId;

                            console.log('AccessToken:' + $scope.accessToken + ', patientId=' + $scope.patient + ', res=' + JSON.stringify(res));

                            var defer = $q.defer();
                            $scope.getPatientInfo(defer);

                        }, "json")
                        .fail(function (error) {
                            console.log('Error getting auth data:' + JSON.stringify(error));
                        });
                }

            });
        };

        $scope.getPatientInfo = function(defer) {
            var url = $scope.settings.serviceUri + "/Patient/" + $scope.settings.patientId;
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + $scope.accessToken);
                }
            })
                .done(function (pt) {
                    $scope.$apply(function () {
                        $scope.patient = pt;
                        console.log('patient = ' + JSON.stringify($scope.patient));
                        var name = pt.name[0].given.join(" ") + " " + pt.name[0].family.join(" ");
                        $scope.patientName = name;
                        console.log('patient = ' + $scope.patientName);
                        $scope.loggedIn = true;
                        defer.resolve();
                    });
                })
                .fail(function (error) {
                    console.log('Error getting patient:' + error + ',' + JSON.stringify(error));
                    defer.reject(-1);
                })
            ;
        };

        $scope.savePulseOximeterData  = function (pId, Sp02, pulse) {
            $scope.message = 'Saving data';

            var jsonO2 = {
                "resourceType": "Observation",
                "text": {
                    "status": "generated",
                    "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, '+pulse+' bpm heart rate</div>'
                },
                "code": {
                    "coding": [
                        {
                            "system": "https://rtmms.nist.gov",
                            "code": "150456",
                            "display": "MDC_PULS_OXIM_SAT_O2"
                        }
                    ]
                },
                "valueQuantity": {
                    "value": Sp02,
                    "units": "percent",
                    "system": "https://rtmms.nist.gov",
                    "code": "262688"
                },
                "appliesDateTime":new Date(),
                "reliability": "ok",
                "subject": {"reference": "Patient/" + $scope.patientId}
            };

            var jsonPulse = {
                "resourceType": "Observation",
                "text": {
                    "status": "generated",
                    "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, '+pulse+' bpm heart rate</div>'
                },
                "code": {
                    "coding": [
                        {
                            "system": "https://rtmms.nist.gov",
                            "code": "149530",
                            "display": "MDC_PULS_OXIM_PULS_RATE"
                        }
                    ]
                },
                "valueQuantity": {
                    "value": pulse,
                    "units": "bpm",
                    "system": "https://rtmms.nist.gov",
                    "code": "264864"
                },
                "appliesDateTime":new Date(),
                "reliability": "ok",
                "subject": {"reference": "Patient/" + $scope.patientId}
            };

            //console.log('Bearer:'+$scope.accessToken);
            //console.log('Saving data:'+JSON.stringify(json));

            $scope.submitObservation(jsonO2);
            $scope.submitObservation(jsonPulse);

        };

        $scope.submitObservation = function(json) {

            // TODO: Move to Angular. See formly-fhir project
            $.ajax({
                type: "POST",
                url: $scope.settings.serviceUri + '/Observation',
                headers: {
                    "Authorization":"Bearer "+$scope.accessToken
                },
                data: JSON.stringify(json),
                success: function (data) {
                    console.log('From server:'+JSON.stringify(data));
                    $scope.$apply(function () {
                        $scope.message = 'Data saved to cloud';
                    });
                },
                datatype: 'json',
                contentType: 'application/json+fhir;charset=utf-8'
            }).fail(function(error) {
                console.log('Save error:'+error+','+JSON.stringify(error));
            }).done(function( data ) {
                console.log( "Returned success from server. Sample of data:"+JSON.stringify(data));
            });
        };

        $scope.submitBundle = function(json) {
            $.ajax({
                type: "POST",
                url: $scope.settings.serviceUri + '/Bundle',
                headers: {
                    "Authorization":"Bearer "+$scope.accessToken
                },
                data: JSON.stringify(json),
                success: function (data) {
                    console.log('From server:'+JSON.stringify(data));
                    $scope.$apply(function () {
                        $scope.message = 'Data saved to cloud';
                    });
                },
                datatype: 'json',
                contentType: 'application/json+fhir;charset=utf-8'
            }).fail(function(error) {
                console.log('Save error:'+error+','+JSON.stringify(error));
            }).done(function( data ) {
                console.log( "Sample of data:"+JSON.stringify(data));
            });
        };


        $scope.saveBP = function (pId, sys, dia) {
            $scope.message = 'Saving data';

            var jsonSys =
                 {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\"> Systolic:'+ sys + '</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "http://loinc.org",
                                "code": "8480-6",
                                "display": "Systolic blood pressure"
                            }
                        ]
                    },
                     "valueQuantity": {
                         "value": sys,
                         "units": "mm[Hg]"
                     },
                    "appliesDateTime":new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.patientId}
                };

            var jsonDia =
            {
                "resourceType": "Observation",
                "text": {
                    "status": "generated",
                    "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">Diastolic:' + dia + '</div>'
                },
                "code": {
                    "coding": [
                        {
                            "system": "http://loinc.org",
                            "code": "8462-4",
                            "display": "Diastolic blood pressure"
                        }
                    ]
                },
                "valueQuantity": {
                    "value": sys,
                    "units": "mm[Hg]"
                },
                "appliesDateTime":new Date(),
                "reliability": "ok",
                "subject": {"reference": "Patient/" + $scope.patientId}
            };

            console.log('Bearer:'+$scope.accessToken);
            console.log('jsonSys:'+JSON.stringify(jsonSys));

            //$scope.submitBundle(json);
            $scope.submitObservation(jsonSys);

            console.log('jsonDia:'+JSON.stringify(jsonDia));
            $scope.submitObservation(jsonDia);


        };

        $scope.saveWeight = function (pId, weight) {
            $scope.message = 'Saving data';

            var json = {
                "resourceType": "Observation",
                "text": {
                    "status": "generated",
                    "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + weight + ' kg</div>'
                },
                "code": {
                    "coding": [
                        {
                            "system": "http://loinc.org",
                            "code": "3141-9",
                            "display": "weight"
                        }
                    ]
                },
                "appliesDateTime":new Date(),
                "valueQuantity": {
                    "value": weight,
                    "units": "kg",
                    "system": "http://unitsofmeasure.org/",
                    "code": "kg"
                },
                "reliability": "ok",
                "subject": {"reference": "Patient/" + $scope.patientId}
            };

            console.log('Bearer:'+$scope.accessToken);
            console.log('Saving data:'+JSON.stringify(json));

            $scope.submitObservation(json);

        };

        $scope.saveSettings = function() {
            Settings.saveSettings();
        };

        $scope.exit = function() {
            app.exitApp();
        };

        $scope.scan = function () {
            BLE.scan();
        };

        $scope.connect = function (connectTo) {

            console.log('Connecting to: ' + connectTo);

            BLE.connect(connectTo);

        };

        $scope.disconnect = function () {
            BLE.disconnect();
            //bleservice.reset();
            $scope.exit();
        };

    }]);
