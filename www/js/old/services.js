/**
 * Created by LarsKristian on 18.08.2015.
 */

'use strict';

/* Services */

var myAppServices = angular.module('fhirApp.services', []);

myAppServices.service('BLE', function ($rootScope, Settings) {

    var BLE = this;
    var myDevice;
    var devices = [];

    console.log('Settings inside BLE = '+JSON.stringify(Settings.getSettings()) );

    var sensorValues = {};
    BLE.getSensorValues = function() {
        return sensorValues;
    };

    var sensorMessage = {};
    BLE.getSensorMessage = function() {
        return sensorMessage;
    };

    BLE.getDevices = function () {
        console.log('Called BLE.deviceList');
        return devices;
    };

    BLE.scan = function () {
        easyble.stopScan();
        easyble.closeConnectedDevices();
        BLE.startScan();
    };

    BLE.disconnect = function () {
        console.log("Stopping scan");
        easyble.closeConnectedDevices();
        easyble.stopScan();
        //evothings.ble.reset();
        if (myDevice != undefined) {
            myDevice.close();
        }
    };

    BLE.reset = function () {
        evothings.ble.reset();
    };

    BLE.startScan = function () {
        easyble.startScan(function (device) {
            // TODO filter the devices according to services provided by them
            console.log('device=' + JSON.stringify(device));
            // Check if device already found
            var arrayLength = devices.length;
            var deviceAlreadyThere = false;
            for (var i = 0; i < arrayLength; i++) {
                if (devices[i].address == device.address) {
                    deviceAlreadyThere = true;
                }
            }
            if (!deviceAlreadyThere) {
                console.log('Adding device: ' + device.name);
                if (typeof device.name === 'undefined' || device.name === null) {
                    device.number = device.address;
                } else {
                    device.number = device.name;
                }
                $rootScope.$apply(function () {
                    devices.push(device);
                });
            } else {
                console.log('Already have device: ' + device.name + ' in ' + JSON.stringify(devices));
            }

            if (device.address == Settings.settings.pulseOximetryDevice) { //'00:1C:05:FF:E1:16') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                BLE.connect(Settings.settings.pulseOximetryDevice);
                sensorValues.deviceType = 'SpO2';
            } else if (device.address == Settings.settings.scaleDevice) { //'5C:31:3E:01:18:A9') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                BLE.connect(Settings.settings.scaleDevice);
                sensorValues.deviceType = 'Scale';
            } if (device.address == Settings.settings.bpDevice) { //'5C:31:3E:01:81:48') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                BLE.connect(Settings.settings.bpDevice);
                sensorValues.deviceType = 'BP';
            }

        }, function (errorCode) {
            console.log('start scan failed');

        });
    };

    BLE.connect = function (connectToAddress) {
        console.log("Stopping scan, connecting to "+connectToAddress);
        //easyble.stopScan();
        //easyble.closeConnectedDevices();

        var arrayLength = devices.length;
        var device;

        for (var i = 0; i < arrayLength; i++) {
            if (devices[i].address == connectToAddress) {
                device = devices[i];
                console.log("Found device");
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Found device: '+device.name;
                });
            }
        }

        //this.connectToDevice(device);
        device.connect(function (device) {
            console.log('Connected - reading services');
            if (sensorValues.deviceType == 'SpO2') {
                BLE.readServicesPulseOximeter(device);
            } else if (sensorValues.deviceType == 'Scale') {
                BLE.readServicesScale(device);
            } else if (sensorValues.deviceType == 'BP') {
                BLE.readServicesBP(device);
            }
            sensorMessage.message = 'Connected to '+device.name;
            //subscribe();
        }, function (errorCode) {
            $rootScope.$apply(function () {
                if (errorCode == 19) {
                    sensorMessage.message = 'Sensor disconnected';
                }
                else {
                    sensorMessage.message = 'Connect error '+errorCode;
                }
            });
            console.log('Connect error: ' + errorCode);
            //BLE.startScan();
            //$rootScope.$broadcast('alertWindow', 'Connection failed. Please try restarting the app and make sure bluetooth is turned on.');
            //evothings.ble.reset();
        });

    };

    BLE.readServicesBP = function (device) {

        myDevice = device;
        device.readServices(['00001810-0000-1000-8000-00805f9b34fb'
        ], function () {
            console.log('Services read');
            //BLE.readBP();
            BLE.subscribeBP();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    /********* BP ****************/

    BLE.subscribeBP = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('00002a35-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([1, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor error: ' + errorCode);
            });

        myDevice.enableNotification('00002a35-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('Notification:'+JSON.stringify(dataArray));
            $rootScope.$apply(function () {
                sensorValues.sys = dataArray[1];
                sensorValues.dia = dataArray[3];
                sensorValues.bp_mean = dataArray[5];
                sensorValues.sys_dia = sensorValues.sys+' / '+sensorValues.dia;
                //sensorValues.pulse = dataArray[14];
                //sensorValues.pulseWithUnit = dataArray[14]+' bpm';
            });
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };


    BLE.readBP = function (defer) {
        myDevice.readCharacteristic('00002a35-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('readScaleData'+JSON.stringify(dataArray));
        }, function (errorCode) {
            console.log('readScaleData error: ' + errorCode);
        });
    };



    /********* SCALE ****************/

    BLE.readServicesScale = function (device) {

        myDevice = device;
        device.readServices(['0000181d-0000-1000-8000-00805f9b34fb'
        ], function () {
            console.log('Services read');
            BLE.subscribeScale();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };


    BLE.subscribeScale = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('00002a9d-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([1, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor error: ' + errorCode);
            });

        myDevice.enableNotification('00002a9d-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('Notification:'+JSON.stringify(dataArray));
            $rootScope.$apply(function () {
                sensorValues.kg = ((dataArray[2]*256)+dataArray[1])*5/1000;
                sensorValues.kgWithUnit = sensorValues.kg+' kg';
                console.log('sensorValues.kg='+sensorValues.kg);
            });
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };


    BLE.readScaleData = function (defer) {
        myDevice.readCharacteristic('00002a9d-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('readScaleData'+JSON.stringify(dataArray));
        }, function (errorCode) {
            console.log('readScaleData error: ' + errorCode);
        });
    };


    /******* OXIMETER ****************/

    BLE.readServicesPulseOximeter = function (device) {

        myDevice = device;
        device.readServices(['46a970e0-0d5f-11e2-8b5e-0002a5d5c51b'
        ], function () {
            console.log('Services read');
            BLE.subscribePulseOximeter();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    // Subscribe summary data
    BLE.subscribePulseOximeter = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([1, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor da184025 error: ' + errorCode);
            });


        myDevice.enableNotification('0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b', function (data) {
            var dataArray = new Uint8Array(data);
            //console.log('Notification:'+JSON.stringify(dataArray));
            if (dataArray[9] != 255 && dataArray[7] != 127) {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Received value from sensor';
                    sensorValues.pulse = dataArray[9];
                    sensorValues.SpO2 = dataArray[7];
                    sensorValues.pulseWithUnit = dataArray[9]+' bpm';
                    sensorValues.SpO2WithUnit = dataArray[7]+' %';
                });
            }
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };

});

myAppServices.service('Settings', function ($rootScope) {

    var Settings = this;
    Settings.settings = {};

    Settings.getSettings = function() {
        if (Settings.settings.patientId == undefined) {
            console.log('Loading settings since they are undefined');
            Settings.loadSettings();
        }
        return Settings.settings;
    };

    Settings.scope = [
        "user/*.*"
        //"patient/*.*",
        //"launch"
    ].join(" ");

    Settings.getScope = function() {
        return Settings.scope;
    };

    Settings.saveSettings = function() {
        console.log('Saving settings: '+JSON.stringify(Settings.settings));
        localStorage.fhirSettings = JSON.stringify(Settings.settings);
    };

    Settings.loadSettings = function() {

        console.log('Loading settings');

        if (localStorage.fhirSettings == undefined) {
            Settings.settings.bpDevice = '5C:31:3E:01:81:48';
            Settings.settings.pulseOximetryDevice = '00:1C:05:FF:E1:16';
            Settings.settings.scaleDevice = '5C:31:3E:01:18:A9';

            Settings.settings.patientId = '1032702';
            Settings.settings.clientId = "samveis";
            Settings.settings.serviceUri = 'http://fhir.roland.bz';
            Settings.settings.authUri = 'http://auth.roland.bz/authorize';
            Settings.settings.tokenUri = 'http://auth.roland.bz/token';
            Settings.settings.redirectUri = 'http://smart.roland.bz/apps/SamveisWebapp/';
            Settings.saveSettings();
            console.log('Wrote new FHIR-settings:'+JSON.stringify(Settings.settings));
        } else {
            Settings.settings = JSON.parse(localStorage.fhirSettings);
        }

        console.log('Settings loaded:'+JSON.stringify(Settings.settings));
    };

});

/*
myAppServices.factory("Observation", function ($resource) {
    return $resource(
        fhirApi+'/Observation',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            // If you want to implement special functions, you'd
            // put them here.
        }
    );
});

myAppServices.factory("ProfileService", function ($resource) {
    return $resource(
        dhisAPI+'/api/me/profile',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            // If you want to implement special functions, you'd
            // put them here.
        }
    );
});



myAppServices.factory("UserSettingService", function ($resource) {
    return $resource(
        dhisAPI+'/api/userSettings/exampleapp.usersetting',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            // If you want to implement special functions, you'd
            // put them here. In this case, the content type cannot be
            // JSON, so we need to change it to text/plain
            save: {
                method:'POST',
                isArray:false,
                headers: {'Content-Type': 'text/plain'}}
        }
    );
});

*/
